# Go Runtime

This runtime is used by the
[`gitlabktl`](http://gitlab.com/gitlab-org/gitlabktl/) tool to build Go
functions.

## How to use it?

You need to implement your function in a `main` package, without the `main()`
function there.

Your function is going to be built as a Go plugin.

### Example

Initialize a new Go module and implement following function in `run.go`:

```bash
$ go mod init gitlab.com/username/go-serverless
$ vim run.go
```

Write a function that will respond with JSON:

```go
package main

import (
	"encoding/json"
	"net/http"
)

func Run(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)

	encoder.Encode(map[string]string{"message": "Hello World!"})
}
```

# Status

This runtime is in _Proof of Concept_ state.

# License

MIT
