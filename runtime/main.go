package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	runtime := Runtime{
		Handler: os.Getenv("FUNCTION_HANDLER"),
	}

	handler, err := runtime.ToHandlerFunc()
	if err != nil {
		log.Panicf("can not load function plugin %v", err)
	}

	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
