package main

import (
	"net/http"
	"plugin"
)

// Runtime represents a Go function provided by user, identified by a
// `handler:` entry from `serverless.yml` that we convert into an environment
// variable during runtime build.
type Runtime struct {
	Handler string
}

// HandleFunc returns a handler that has been found in the function plugin
func (r Runtime) ToHandlerFunc() (http.HandlerFunc, error) {
	p, err := r.toPlugin()
	if err != nil {
		return nil, err
	}

	symbol, err := p.Lookup(r.Handler)
	if err != nil {
		return nil, err
	}

	return symbol.(func(http.ResponseWriter, *http.Request)), nil
}

func (r Runtime) toPlugin() (*plugin.Plugin, error) {
	return plugin.Open("function.so")
}
